#include <QApplication>
#include <QSystemTrayIcon>
#include <QIcon>
#include <QMenu>
#include <QDebug>
#include "MenuViewHandler.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    // creating tray icon
    QSystemTrayIcon *trayIcon = new QSystemTrayIcon();
    trayIcon->setIcon(QIcon(":/smile.jpg"));
    trayIcon->show();

    // add menu
    MenuViewHandler *menuHandler = new MenuViewHandler();
    menuHandler->openFile("../../../../TIM/menu.xml"); // from build dir to code dir
    trayIcon->setContextMenu(menuHandler->menu("LoggedIn"));

    return a.exec();
}
