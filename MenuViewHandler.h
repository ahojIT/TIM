#ifndef MENUVIEWHANDLER_H
#define MENUVIEWHANDLER_H

#include <QObject>
#include <QHash>
#include <QIODevice>
#include <QDomNode>
#include <QMenu>

class MenuViewHandler : public QObject
{
    Q_OBJECT
public:
    explicit MenuViewHandler(QObject *parent = nullptr);
    bool openFile(const QString &menuPath);
    QMenu *menu(const QString &state);

signals:

public slots:

private:
    bool parseStates(QIODevice *file);
    QMenu *traverseXmlNode(const QDomNode &node);
    QMenu *createSubmenu(const QDomNamedNodeMap &attrs);
    QAction *createAction(const QDomNamedNodeMap &attrs);

private:
    QHash<QString, QDomNode> _stateMenuHash;
};

#endif // MENUVIEWHANDLER_H
