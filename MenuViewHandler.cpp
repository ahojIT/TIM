#include "MenuViewHandler.h"
#include <QFileInfo>
#include <QtXml>
#include <QDebug>

#define TAG_STATE           "state"
#define TAG_MENU            "menu"
#define TAG_ACTION          "action"
#define TAG_SEPARATOR       "separator"
#define STATE_ATTR_NAME     "name"
#define MENU_ATTR_NAME      "name"
#define ACTION_ATTR_NAME    "name"
#define ACTION_ATTR_ENABLED "enabled"

MenuViewHandler::MenuViewHandler(QObject *parent) : QObject(parent)
{

}

bool MenuViewHandler::openFile(const QString &menuPath)
{
    QFile file(menuPath);

    if(file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        if(!parseStates(&file)) {
            file.close();
            return false;
        }
        file.close();
        return true;
    }
    else
        qDebug() << "Error: Cannot open menu file " << menuPath;

    return false;
}

QMenu *MenuViewHandler::menu(const QString &state)
{
    QMenu *menu = nullptr;
    if(_stateMenuHash.keys().contains(state)) {
        menu = traverseXmlNode(_stateMenuHash.value(state));
    }
    else {
        menu = new QMenu();
        QAction *actionExit = new QAction("Exit");
        QObject::connect(actionExit, SIGNAL(triggered()), QCoreApplication::instance(), SLOT(quit()));
        menu->addAction(actionExit);
    }

    return menu;
}

bool MenuViewHandler::parseStates(QIODevice *file)
{
    QDomDocument doc;

    QString errorMsg;
    int errorLine;
    int errorColumn;

    if(doc.setContent(file, &errorMsg, &errorLine, &errorColumn))
    {
        QDomElement docElem = doc.documentElement();
        QDomNode n = docElem.firstChild();

        // find every <state>
        while(!n.isNull()) {
            QDomElement e = n.toElement();
            if(!e.isNull() && e.tagName() == TAG_STATE) {
                QString statesString = n.toElement().attribute(STATE_ATTR_NAME);
                QStringList statesList = statesString.split(",");
                for(QString state : statesList)
                    _stateMenuHash.insert(state.trimmed(), n); // save states node for every state
            }
            n = n.nextSibling();
        }

        if(_stateMenuHash.isEmpty()) {
            qDebug() << "Error: No state found";
            return false;
        }

        qDebug() << "Found" << _stateMenuHash.size() << "states:" << _stateMenuHash.keys();
        return true;
    }

    qDebug() << "Parser error:" << errorMsg << "Line:" << errorLine << "Column:" <<  errorColumn;
    return false;
}

QMenu *MenuViewHandler::traverseXmlNode(const QDomNode &node)
{
    QMenu *menu = new QMenu();
    QAction *lastSubmenu = nullptr;

    QDomNode domNode = node.firstChild();
    QDomElement domElement;

    // go thru all tags
    while(!(domNode.isNull()))
    {
        // if current node contains known tag element, add this tag (menu, action, separator) to menu
        if(domNode.isElement())
        {
            domElement = domNode.toElement();
            if(!(domElement.isNull()))
            {
                QString tag = domElement.tagName();
                QDomNamedNodeMap attrs = domElement.attributes();

                if(tag == TAG_MENU)
                    lastSubmenu = menu->addMenu(createSubmenu(attrs));
                else if(tag == TAG_ACTION)
                    menu->addAction(createAction(attrs));
                else if(tag == TAG_SEPARATOR)
                    menu->addSeparator();
                else
                    qDebug() << "unhandled tag" << tag;
            }
        }

        // call this function recursive
        QMenu *subMenu = traverseXmlNode(domNode);

        // move everything from subMenu to last added menu
        if(lastSubmenu && subMenu->actions().size() > 0)
            lastSubmenu->menu()->addActions(subMenu->actions());

        // delete unnecessary subMenu
        //delete subMenu; // Why it deletes all separators?

        // go to next sibling
        domNode = domNode.nextSibling();
    }

    return menu;
}

QMenu *MenuViewHandler::createSubmenu(const QDomNamedNodeMap &attrs)
{
    QMenu *menu = new QMenu();

    for(int i=0; i<attrs.length(); i++)
    {
        QDomAttr attr = attrs.item(i).toAttr();
        if(attr.name() == MENU_ATTR_NAME)
            menu->setTitle(attr.value());
    }

    return menu;
}

QAction *MenuViewHandler::createAction(const QDomNamedNodeMap &attrs)
{
    QAction *action = new QAction();

    for(int i=0; i<attrs.length(); i++)
    {
        QDomAttr attr = attrs.item(i).toAttr();
        if(attr.name() == ACTION_ATTR_NAME)
            action->setText(attr.value());
        else if(attr.name() == ACTION_ATTR_ENABLED)
            action->setEnabled(attr.value().toLower() == QString("true"));
    }

    return action;
}


















